import os
import argparse
from copy import deepcopy

from data_generator import ds_name_mask, Config, LastSeqGen, generate_population_size_config, generate_nn_size_config,\
    generate_dataset_size_config, generate_word_size_config


def generate_experiments(default_config, exp_dir, dataset_dir):
    # nn_size
    nn_size_dir = os.path.join(exp_dir, "nn_size")
    os.mkdir(nn_size_dir)
    config = deepcopy(default_config)
    nn_size_range = [2, 4, 8, 16, 24, 32, 100, 200, 300, 400, 500]
    generate_nn_size_config(config, nn_size_range, nn_size_dir)

    # dataset_size
    dataset_size_dir = os.path.join(exp_dir, "dataset_size")
    if not os.path.isdir(dataset_size_dir):
        os.mkdir(dataset_size_dir)
    config = deepcopy(default_config)
    dataset_size_range = range(100, 1001, 100)
    word_len = 100
    generate_dataset_size_config(config, dataset_size_range, word_len, dataset_dir, dataset_size_dir)

    # word_size
    word_size_dir = os.path.join(exp_dir, "word_size")
    if not os.path.isdir(word_size_dir):
        os.mkdir(word_size_dir)
    config = deepcopy(default_config)
    word_size_range = range(100, 1001, 100)
    ds_size = 100
    generate_word_size_config(config, word_size_range, ds_size, dataset_dir, word_size_dir)

    # pop_size
    pop_size_dir = os.path.join(exp_dir, "pop_size")
    if not os.path.isdir(pop_size_dir):
        os.mkdir(pop_size_dir)
    config = deepcopy(default_config)
    pop_size_range = range(10, 101, 10)
    generate_population_size_config(config, pop_size_range, pop_size_dir)


def generate_datasets(dataset_dir):
    generator = LastSeqGen()
    # for dataset size experiment
    dataset_size_range = range(100, 1001, 100)
    word_len = 100
    for ds_size in dataset_size_range:
        output = os.path.join(dataset_dir, ds_name_mask.format(num_smpl=ds_size, word_len=word_len))
        if not os.path.isfile(output):
            generator.generate_dataset(ds_size, word_len, save=output)

    # for word size experiment
    word_size_range = range(100, 1001, 100)
    ds_size = 100
    for word_len in word_size_range:
        output = os.path.join(dataset_dir, ds_name_mask.format(num_smpl=ds_size, word_len=word_len))
        if not os.path.isfile(output):
            generator.generate_dataset(ds_size, word_len, save=output)
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('ds_path', help='path to folder for generated dataset storage')
    parser.add_argument('exp_path', help='path to folder for generated configs storage')
    parser.add_argument('--relative', help='make dataset path in configs be relative to given dir')
    args = parser.parse_args()

    ds_path = args.ds_path
    if not os.path.isdir(ds_path):
        os.mkdir(ds_path)
    if not os.path.isdir(ds_path):
        raise ValueError("Error! Dataset store folder not exists: %s" %ds_path)
    exp_path = args.exp_path
    if not os.path.isdir(exp_path):
        os.mkdir(exp_path)
    if not os.path.isdir(exp_path):
        raise ValueError("Error! Config store folder not exists: %s" %exp_path)

    generate_datasets(ds_path)
    default_ds = os.path.join(ds_path, ds_name_mask.format(num_smpl=100, word_len=100))
    config = Config(default_ds, psize=10, semiring="tropical")
    generate_experiments(config, exp_path, ds_path)

import argparse
import os
from shutil import rmtree
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['figure.autolayout'] = True
markersize=150
# types of runtime
key_ser = 'serialized'
key_par = 'parallelized'
key_nof = 'no_feature'
key_sem_cl = 'classic'
key_sem_tr = 'tropical'
colors = {key_ser: '0.4',
          key_par: '0.8',
          key_nof: 'cyan'}
shape = {key_ser: 'P',
         key_par: '8',
         key_sem_cl: 'o',
         key_sem_tr: '^'}

# types of experiment
exp_nn = 'nn size'
exp_ds = 'dataset size'
exp_ws = 'word size'
exp_pp = "population size"
exp_al = 'alphabet size'
sem_tp = 'classic vs tropical'


def create_nn_size_plot(data, outdir=None):
    fig, ax = plt.subplots(figsize=(8,6))
    for label, df in data.groupby('type'):
        df.plot.scatter(x='SNN size', y='average epoch time, s', c=colors[label], marker=shape[label], s=markersize
                        , ax=ax, label=label, title="Processing time over SNN size")
    plt.legend()

    if outdir:
        out_plt = os.path.join(outdir, 'nn_size_exp.jpg')
        plt.savefig(out_plt)
        plt.close()
    else:
        plt.show()


def create_semiring_type_plot(data, outdir=None):
    fig, ax = plt.subplots(figsize=(8, 6))
    for label, df in data.groupby('type'):
        for label_sem, df in df.groupby('semiring'):
            df.plot.scatter(x='word length', y='average epoch time, s', c=colors[label], marker=shape[label_sem]
                        , s=markersize, ax=ax, label="%s_%s" %(label, label_sem)
                        , title="Processing time over semiring type")
    plt.legend()
    if outdir:
        out_plt = os.path.join(outdir, 'semiring_type_exp.jpg')
        plt.savefig(out_plt)
        plt.close()
    else:
        plt.show()


def create_word_size_plot(data, outdir=None):
    fig, ax = plt.subplots(figsize=(8, 6))
    for label, df in data.groupby('type'):
        df.plot.scatter(x='word length', y='average epoch time, s', c=colors[label], marker=shape[label], s=markersize
                        , ax=ax, label=label, title="Processing time over word length")
    plt.legend()

    if outdir:
        out_plt = os.path.join(outdir, 'word_size_exp.jpg')
        plt.savefig(out_plt)
        plt.close()
    else:
        plt.show()


def create_dataset_size_plot(data, outdir=None):
    fig, ax = plt.subplots(figsize=(8, 6))
    for label, df in data.groupby('type'):
        df.plot.scatter(x='dataset size', y='average epoch time, s', c=colors[label], marker=shape[label], s=markersize
                        , ax=ax, label=label, title="Processing time over dataset size")
    plt.legend()

    if outdir:
        out_plt = os.path.join(outdir, 'dataset_size_exp.jpg')
        plt.savefig(out_plt)
        plt.close()
    else:
        plt.show()


def create_alphabet_size_plot(data, outdir=None):
    fig, ax = plt.subplots(figsize=(8, 6))
    for label, df in data.groupby('type'):
        df.plot.scatter(x='alphabet', y='average epoch time, s', c=colors[label], marker=shape[label], s=markersize
                        , ax=ax, label=label, title="Processing time over alphabet length")
    plt.legend()

    if outdir:
        out_plt = os.path.join(outdir, 'alphabet_size_exp.jpg')
        plt.savefig(out_plt)
        plt.close()
    else:
        plt.show()


def create_population_size_plot(data, outdir=None):
    fig, ax = plt.subplots(figsize=(8, 6))
    for label, df in data.groupby('type'):
        df.plot.scatter(x='population size', y='average epoch time, s', c=colors[label], marker=shape[label], s=markersize
                        , ax=ax, label=label, title="Processing time over population size")
    plt.legend()

    if outdir:
        out_plt = os.path.join(outdir, 'population_size_exp.jpg')
        plt.savefig(out_plt)
        plt.close()
    else:
        plt.show()


def format_google_sheet(df):
    db['average epoch time, s'] = db['average epoch time, s'].str.replace(',', '.').astype(float)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--db', help='path to database', default='Poster-GenitorII.csv')
    parser.add_argument('--nn', action='store_true', help='create nn size experiment plot')
    parser.add_argument('--ds', action='store_true', help='create dataset size experiment plot')
    parser.add_argument('--ws', action='store_true', help='create word size experiment plot')
    parser.add_argument('--pp', action='store_true', help='create population size experiment plot')
    parser.add_argument('--al', action='store_true', help='create alphabet size experiment plot')
    parser.add_argument('--sem', action='store_true', help='create semiring type experiment plot')
    parser.add_argument('--save', action='store_true', help='save output plots')
    args = parser.parse_args()
    if not os.path.exists(args.db):
        raise ValueError("Error! Database file not exists: ", args.db)
    db = pd.read_csv(args.db)
    format_google_sheet(db)
    outdir = None
    if args.save:
        outdir = 'out_plots'
        if os.path.isdir(outdir):
            rmtree(outdir)
        os.mkdir(outdir)
    if args.nn:
        create_nn_size_plot(db[exp_nn == db['experiment']], outdir=outdir)
    if args.ds:
        create_dataset_size_plot(db[exp_ds == db['experiment']], outdir=outdir)
    if args.ws:
        create_word_size_plot(db[exp_ws == db['experiment']], outdir=outdir)
    if args.pp:
        create_population_size_plot(db[exp_pp == db['experiment']], outdir=outdir)
    if args.al:
        create_alphabet_size_plot(db[exp_al == db['experiment']], outdir=outdir)
    if args.sem:
        create_semiring_type_plot(db[sem_tp == db['experiment']], outdir=outdir)


import os
import argparse
import numpy as np

MAX_WORD_LEN = 1000
ds_name_mask = "ds-size_{num_smpl}-wl_{word_len}.json"
cfg_name_mask = "config-{exp}-{id}.json"

class Generator:
    alphabet = []

    def generate(self, wlen):
        return [],

    def calc_gt(self, sample):
        return -1


class LastSeqGen(Generator):
    def __init__(self):
        self.alphabet = ['X', 'o']
        self.alen = len(self.alphabet)

    def generate(self, wlen):
        if wlen < 1:
            wlen = np.random.random_integers(low=1, high=MAX_WORD_LEN)
        sample = [self.alphabet[np.random.random_integers(low=0, high=self.alen - 1)] for i in range(wlen)]
        return sample, self.calc_gt(sample)

    def calc_gt(self, sample):
        res = 0
        start = False
        for idx in range(len(sample) - 1, 0, -1):
            if sample[idx] != self.alphabet[0]:
                if start:
                    break
                else:
                    continue
            start = True
            res += 1
        return res

    def str_dataset(self, dataset):
        str_dataset = '{\n'
        # population params
        str_dataset += '"DatasetFormat" : "synthetic",\n'
        alphabet_str = ', '.join(['"%s"' % ch for ch in self.alphabet])
        str_dataset += '"alphabet": [%s],\n' %alphabet_str
        data_str = ', '.join(['"%s"' % word for word in dataset["data"]])
        str_dataset += '"trainingData": [%s],\n' % data_str
        str_dataset += '"testData": [%s]\n' % ', '.join([str(word) for word in dataset["gt"]])
        str_dataset += "}"
        return str_dataset

    def generate_dataset(self, nsamples, wlen, save=None):
        dataset = {"data": [],
                   "gt": []}
        for i in range(nsamples):
            data, gt = self.generate(wlen)
            dataset["data"].append("".join(data))
            dataset["gt"].append(gt)
        if save is not None:
            with open(save, 'w') as f:
                f.write(self.str_dataset(dataset))
        return dataset


class FullAlphabetGen(Generator):

    def __init__(self, alpha_len=-1):
        import string
        self.alen = min(alpha_len, len(string.ascii_letters)) if alpha_len > 0 else len(string.ascii_letters)
        self.alphabet = [ch for ch in string.ascii_letters[:self.alen]]
        #self.alen = min(alpha_len, 256)
        #self.alphabet = [chr(val) for val in range(self.alen)]

    def generate(self, wlen):
        if wlen < 1:
            wlen = np.random.random_integers(low=1, high=MAX_WORD_LEN)
        sample = [self.alphabet[np.random.random_integers(low=0, high=self.alen - 1)] for i in range(wlen)]
        return sample, self.calc_gt(sample)

    def calc_gt(self, sample):
        res = 0
        start = False
        for idx in range(len(sample) - 1, 0, -1):
            if sample[idx] != self.alphabet[0]:
                if start:
                    break
                else:
                    continue
            start = True
            res += 1
        return res


class Config:
    def __init__(self, ds_path, psize=20, nnsize=2, semiring="tropical", nnmin=0, nnmax=2):
        self.semiring = semiring
        self.psize = psize
        self.nnsize = nnsize
        self.nnmin = nnmin
        self.nnmax = nnmax
        self.dataset_path = ds_path

    def save(self, out_path):
        with open(out_path, 'w') as f:
            f.write(str(self))

    def __str__(self):
        str_config = '{\n'
        # population params
        str_config += '"popSize": %d,\n' % self.psize
        str_config += '"nnSize": %d,\n' % self.nnsize
        str_config += '"min": %d,\n' % self.nnmin
        str_config += '"max": %d,\n' % self.nnmax
        str_config += '"Semiring": "%s",\n' % self.semiring
        str_config += '"dataset": "%s"\n' % self.dataset_path.replace('\\', '/')
        str_config += "}"
        return str_config


def generate_word_size_config(cfg, word_size_range, ds_size, dataset_dir, out_folder):
    exp_name = "word_size"
    for ws in word_size_range:
        filename = os.path.join(out_folder, cfg_name_mask.format(exp=exp_name, id=ws))
        cfg.dataset_path = os.path.join(dataset_dir, ds_name_mask.format(num_smpl=ds_size, word_len=ws))
        cfg.save(filename)


def generate_dataset_size_config(cfg, dataset_range, word_len, dataset_dir, out_folder):
    exp_name = "dataset_size"
    for ds_size in dataset_range:
        filename = os.path.join(out_folder, cfg_name_mask.format(exp=exp_name, id=ds_size))
        cfg.dataset_path = os.path.join(dataset_dir, ds_name_mask.format(num_smpl=ds_size, word_len=word_len))
        cfg.save(filename)


def generate_nn_size_config(cfg, nnsize_range, out_folder):
    exp_name = "nn_size"
    for nnsize in nnsize_range:
        filename = os.path.join(out_folder, cfg_name_mask.format(exp=exp_name, id=nnsize))
        cfg.nnsize = nnsize
        cfg.save(filename)


def generate_population_size_config(cfg, pop_range, out_folder):
    exp_name="pop_size"
    for pop_size in pop_range:
        filename = os.path.join(out_folder, cfg_name_mask.format(exp=exp_name, id=pop_size))
        cfg.psize = pop_size
        cfg.save(filename)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('out_path', help='path to output folder')
    parser.add_argument('--num_samples', '-s', type=int, default=50,
                        help='num train samples')
    parser.add_argument('--word_length', '-l', type=int, default=-1, 
                        help='num train samples')
    parser.add_argument('--nn_size', '-n', type=int, default=2,
                        help='size of nn model')
    parser.add_argument('--alphabet_mode', '-a', type=int, default=0,
                        help='size of nn model')
    parser.add_argument('--semiring', '-m', type=str, default='tropical',
                        help='semiring type')
    args = parser.parse_args()

    gen = FullAlphabetGen(args.alphabet_mode) if args.alphabet_mode is not 0 else LastSeqGen()
    config = Config(gen, nnsize=args.nn_size, semiring=args.semiring)
    config.generate_data(args.num_samples, args.word_length)
    config.save(args.out_path)

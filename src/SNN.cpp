#include <string>
#include <cmath>
#include <iostream>
#include <execution>

#include <torch/torch.h>

#include "SNN.h"

namespace genitor2 {

    /**
    * Constructor for Equations objects
    * @param id an id only used for test purposes which should be setup by the calling instance to make sure it is unique
    * @param min, max weights may be drawn as ints from [min, max]
    * @param size the size of the corresponding SNN
    * @param trainingDataId Id for the training data arguments
    * @param testDataId Id for the training data output
    * */
    SNN::SNN(int id, const Config &_config)
        : id(id)
        , numStates(_config.nnSize)
        , alphabetSize(_config.ds->abc().size())
    {
        weightsSize = alphabetSize * numStates * numStates;
        binaryLength = floor(log2(_config.max) + 1);
        maxNum = (int) pow(2, binaryLength) - 1;
        weights = randomMatrix(alphabetSize, _config.min, _config.max, numStates);
        linear = torch::empty(weightsSize, torch::kLong);
        int m = 0;
        auto weights_a = weights.accessor<int64_t, 3>();
        std::vector<int> tempBinary((weightsSize * binaryLength), 0);
        for (int j = 0; j < alphabetSize; j++) {
            for (int k = 0; k < numStates; k++) {
                for (int l = 0; l < numStates; l++) {
                    convertIntToBinary(weights_a[j][k][l], binaryLength, (m * binaryLength), tempBinary);
                    linear[m] = weights_a[j][k][l];
                    m++;
                }
            }
        }
        binary = torch::tensor(tempBinary, torch::kByte);
        
        semiring = mSemiringSelector.getSemiring(_config.semiring);
        if (nullptr == semiring)
        {
            semiring = mSemiringSelector.getSemiring(SemiringType::classic);
            std::cout << "Error! Semiring is not found: " << mSemiringSelector.getSemiringName(_config.semiring)
                << ". Will be used " << mSemiringSelector.getSemiringName(semiring->getType()) << std::endl;
        }
    }

    /**
    * Converts an unsigned integer to binary representation (leftmost position is most significant bit).
    * @param int the (unsigned) integer to be converted.
    * @param bits the number of bits used to represent the integer in binary. Must be high enough to properly store the entire number
    * @param pos pos in the vector where the binary representation will be inserted
    * @param array the binary representation will be stored in this vector
    * @return the array containing the binary representation of the given unsigned integer
    * */
    void SNN::convertIntToBinary(unsigned int num, int bits, int pos, std::vector<int> &arr) {
        for (int i = 0; (num != 0) && (i < bits); i++) {
            arr[pos + i] = num % 2;
            num = num / 2;
        }
    }

    /**
    * Converts the binary chromosome (stored in binary) to decimal representation
    * */
    void SNN::convertToDecimal() {
        std::vector<int> temp(this->binary.size(0) / this->binaryLength);
        auto binaryAccessor = this->binary.accessor<uint8_t, 1>();

        int i = 0, j = 0, k = 0;
        int binaryPos = 0;

        for (int m = 0; m < temp.size(); m++) {
            int num = 0;
            int exp = this->binaryLength - 1;
            for (int n = 0; n < this->binaryLength; n++) { // go through one number in binary representation

                if (exp > 0) num += binaryAccessor[binaryPos] * (pow(2, exp)); // binaryLength > 1
                else num += binaryAccessor[binaryPos]; // binaryLength = 1

                --exp;
                ++binaryPos;
            }

            temp[m] = num;
            this->weights[i][j][k] = num;

            ++k;
            if (k >= this->numStates) {
                k = 0;
                ++j;
                if (j >= this->numStates) {
                    j = 0;
                    if (i < alphabetSize - 1) {
                        ++i;
                    }
                }
            }
        }

        this->linear = torch::tensor(temp, torch::kInt);
    }

    void SNN::convertToBinary() {
        std::vector<int> temp((linear.size(0) * this->binaryLength), 0);
        auto target_linear_a = linear.accessor<int64_t, 1>();
        for (int i = 0; i < linear.size(0); i++) {
            SNN::convertIntToBinary(target_linear_a[i], this->binaryLength, (i * this->binaryLength), temp);
        }
        binary = torch::tensor(temp, torch::kByte);
    }


    /**
    * Set the semiring to s.
    * @param s the semiring to be used
    * */
    void SNN::setSemiring(Semiring* s) {
        semiring = s;
    }

    /**
    * Generate weight matrix with random integer values.
    * Initialise weights with random values in the range [min,max].
    * @param alpha the size of the alphabet
    * @param min the smallest possible random value chosen
    * @param max the highest possible random value chosen
    * @param n the size of the SNN
    * @return w the weight matrix with random entries
    * */
    torch::Tensor SNN::randomMatrix(int alpha, int min, int max, int n) {
        torch::Tensor w = torch::randint(min, max, {alpha, n, n}, torch::kLong);
        return w;
    }

    /**
    * Generate weight matrix with all weights equal to zero
    * @return w the weight matrix
    * */
    torch::Tensor SNN::zeroMatrix(int alpha, int n) {
        return torch::zeros({alpha, n, n}, torch::kLong);
    }

    /*
    * Generate weight matrix with all weights equal to one
    * @return w the weight matrix
    * */
    torch::Tensor SNN::oneMatrix(int alpha, int n) {
        return torch::ones({alpha, n, n}, torch::kLong);
    }

    /**
    * Set the weight matrix between the gate and the state layer of the neural net.
    * The weight matrix must have the format in the format { ["a"] = {{j1,j2,...,jn},{m1,m2,...mn}}},
    * to indicate that from gate neuron (a,1) to state neuron 1, the weight has value
    * j1, from (a,1) to 2, value j2, ..., from (a,2) to n, value mn etc.
    * @param weightTensor the weight tensor where entry [i][j][k] corresponds to weight of symbol at index i from node j to node k
    * @see weightsCountAs
    * */
    void SNN::setWeights(torch::Tensor weightTensor) {
        weights = weightTensor;
    }

    /**
    * @return the weight tensor where entry[i][j][k] corresponds to weight of symbol at index i from node j to node k
    * */
    torch::Tensor SNN::getWeights() {
        return weights;
    }

    /**
    * Adjust the weights so that they correspond to their linear representation
    * */
    void SNN::setWeightsFromLinear() {
        int m = 0;
        for (int i = 0; i < alphabetSize; i++) {
            for (int j = 0; j < numStates; j++) {
                for (int k = 0; k < numStates; k++) {
                    weights[i][j][k] = linear[m];
                    m++;
                }
            }
        }
    }

    /**
    * Set the core size of the corresponding SNN.
    * @param stateNum the desired size. Must be > 0, otherwise set size to 1.
    * */
    void SNN::setNumberOfStates(int stateNum) {
        numStates = (stateNum > 0) ? stateNum : 1;
    }

    int SNN::getNumberOfStates() {
        return numStates;
    }

    // Compute the value of state neuron i after processing a word.
    // @param i the neuron corresponding to state i
    // @param word a string
    // @return the value of state i after processing word
    double SNN::getStateValue(int i, const std::string &word) {
        double stateVal = 0;
        if (word.empty()) {
            if (i == 1) { // if true then "and" returns second part aka semiring->one
                //if in addition semiring->one is true then "or" rewturns that
                //in addition lua just see "false" and "null" as false everything else is true
                stateVal = semiring->one() != NULL ? semiring->one() : semiring->zero();
            } else { stateVal = (i == 1); }//if i==1 is false then and would return it as proof why "and" fails
        } else if (word.length() == 1) {
            stateVal = this->weights[word[0]][1][i].item<double>();
        } else {
            std::string stem = word.substr(0, word.length() - 2);
            char suffix = word[word.length() - 1];
            if (0 < i && i <= this->numStates) {
                for (int j = 0; j < this->numStates; j++) {
                    stateVal = semiring->add(stateVal, semiring->mult(
                            this->weights[suffix][j][i].item<double>(),
                            this->getStateValue(j, stem)));
                }
            }
        }
        return stateVal;
    }

    // Performs a forward pass and returns the output of the neural net, asssuming it is normalised.
    // @param word the word to perform the forward pass for
    // @return the value after performing a forward pass on input word
    double SNN::forward(const std::string &word) {
        long length = (long) word.length();
        torch::Tensor states = torch::zeros({length + 1, this->numStates}, torch::kLong);
        // Alternative: torch::empty(word.length()+1, this->size, torch::kLong);
        states[0] = torch::zeros(this->numStates, torch::kLong);
        states[0][0] = semiring->one();
        for (int t = 1; t < word.length() + 1; t++) {

            //std::cout << states[t - 1] << std::endl;
            //std::cout << states[t - 1].contiguous().view({this->size, 1}).transpose(0, 1) << std::endl;

            states[t] = (semiring->tensormult(states[t - 1].contiguous().view({this->numStates, 1}).transpose(0, 1),
                                              this->weights[word[t - 1]])).view(this->numStates);
        }
        return states[word.length()][this->numStates - 1].item<double>();
    }

    // Compute the mean squared error of the net for the entire training data.
    //@return the MSE for the current training data
    float SNN::squaredError(const std::vector<std::string> &convertedData, const std::vector<float> &testData) {
        double sum = 0;
        std::vector<std::pair<const std::string*, float>> processData;
        processData.reserve(convertedData.size());
        for (int i = 0; i < convertedData.size(); i++)
        {
            processData.push_back(std::pair<const std::string*, float>(&(convertedData[i]), testData[i]));
        }

        std::for_each(
            std::execution::par,
            processData.begin(),
            processData.end(),
            [&](auto&& example)
            {
                double err = (example.second - forward(*example.first));
                sum += err * err;
            });
        return sum / convertedData.size();
    }

    //@alpha the size of the alphabet
    //Print weights in a readable format.
    void SNN::printWeights(int alpha) {
        for (int i = 0; i < alpha; i++) {
            for (int j = 0; j < this->numStates; j++) {
                for (int k = 0; k < this->numStates; k++) {
                    std::cout << "Weight (" << i << ", " << j << ") -> " << " : " << this->weights[i][j][k]
                              << std::endl;
                }
            }
        }
    }

    //Print automaton to console.
    void SNN::printAutomaton() {
        std::cout << "Automaton has " << this->numStates << " states " << std::endl;
        std::cout << "Transitions (state j) --symbol--> (state k): weight" << std::endl;
        for (int i = 0; i < alphabetSize; i++) {
            for (int j = 0; j < this->numStates; j++) {
                for (int k = 0; k < this->numStates; k++) {
                    std::cout << "(" << j << ") --" << i << "--> (" << k << ") : "
                              << this->weights[i][j][k] << std::endl;
                }
            }
        }
    }

    // Export the automaton to a *.gv file to later draw it.
    //@name the file name as string (without extension).
    void SNN::exportAutomatonToFile(const std::string &name) {
        std::ofstream myFile;
        myFile.open(name + ".gv");
        myFile << "digraph " << name << " {\n\trankdir=LR\n\tl -> q1 [label=\"" << semiring->one() << "\"];\n";
        std::string label = "";
        for (int j = 0; j < this->numStates; j++) {
            for (int k = 0; k < this->numStates; k++) {
                label = "";
                for (int i = 0; i < alphabetSize; i++) {
                    if ((this->weights[i][j][k].item<double>() != semiring->zero()) ||
                        (semiring->zero() == INT32_MIN && this->weights[i][j][k].item<double>() < -1000000000)) {
                        label = label + std::to_string(i) + "/" +
                                std::to_string(this->weights[i][j][k].item<double>()) + "\n";
                    }

                }
                if (label != "") {
                    myFile << "\tq" << j << " -> q" << k << " [label=\"" << label << "\"];\n";
                }
            }
        }
        myFile << "\tq" << this->numStates << " -> g [label=\"" << semiring->one()
               << "\"];\n\tl [style = invis];\n\tg [style = invis];\n}";
        myFile.close();
    }


    std::string exec(const char *cmd) {
        std::array<char, 128> buffer;
        std::string result;
#ifdef _WIN32
        std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"),
                                                      _pclose);
#else
        std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"),
            pclose);
#endif
        if (!pipe) {
            throw std::runtime_error("popen() failed!");
        }
        while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
            result += buffer.data();
        }
        return result;
    }

    // Export automaton to a *.gv file and produce a png file with the drawn automaton
    //@name the file name as string (without extension)
    void SNN::drawAutomaton(const std::string &name) {
        this->exportAutomatonToFile(name);
        std::string cmd = "dot -Tpng " + name + ".gv -o " + name + ".png";
        exec(cmd.c_str());
    }

    // Print values of the states (nodes) after processing a word
    //@param word the word to process
    void SNN::printStatesAfter(const std::string &word) {
        for (int i = 0; i < this->numStates; i++) {
            std::cout << "q(" << i << ", " << word << "): " << this->getStateValue(i, word) << std::endl;
        }
    }

    SNN::~SNN() {

    }
}

#include <fstream>
#include <algorithm>

#include "DataLoader.h"

namespace genitor2 {
    Dataset::Dataset(const std::string& datafile)
        : loaded(false)
    {
        std::ifstream file(datafile);
        if (!file)
        {
            std::cout << " Error! Can't load dataset file: " << datafile << std::endl;
            return;
        }
        // parse json data
        nlohmann::json json_data;
        try
        {
            file >> json_data;
        }
        catch (...)
        {
            std::cout << " Error! Can't load json file: incorrect data structure in file" << std::endl;
            return;
        }
        
        try
        {
            alphabet.reserve(json_data["alphabet"].size());
            for (const auto& data : json_data["alphabet"])
            {
                alphabet.push_back(std::string(data)[0]);
            }
            sort(alphabet.begin(), alphabet.end());

            trainingData.reserve(json_data["trainingData"].size());
            for (const auto& data : json_data["trainingData"])
            {
                trainingData.push_back(data);
            }
            convertData();

            testData.reserve(json_data["testData"].size());
            for (const auto& data : json_data["testData"])
            {
                testData.push_back(data);
            }

            if (testData.size() == trainingData.size() && testData.size() == convertedData.size())
            {
                loaded = true;
            }
        }
        catch (...)
        {
            std::cout << " Error! Can't load dataset data: missed required fields" << std::endl;
        }
    }

    Dataset::Dataset(const nlohmann::json& json_data)
        : loaded(false)
    {
        try
        {
            alphabet.reserve(json_data["alphabet"].size());
            for (const auto& data : json_data["alphabet"])
            {
                alphabet.push_back(std::string(data)[0]);
            }
            sort(alphabet.begin(), alphabet.end());

            trainingData.reserve(json_data["trainingData"].size());
            for (const auto& data : json_data["trainingData"])
            {
                trainingData.push_back(data);
            }
            convertData();

            testData.reserve(json_data["testData"].size());
            for (const auto& data : json_data["testData"])
            {
                testData.push_back(data);
            }

            if (testData.size() == trainingData.size() && testData.size() == convertedData.size())
            {
                loaded = true;
            }
        }
        catch (...)
        {
            std::cout << " Error! Can't load dataset data: missed required fields"  << std::endl;
        }
    }

    Dataset::Dataset(std::vector<char> _alphabet, std::vector<std::string> _trainingData, std::vector<float> _testData)
        : alphabet(std::move(_alphabet))
        , trainingData(std::move(_trainingData))
        , testData(std::move(_testData))
        , loaded(true)
    {
        sort(alphabet.begin(), alphabet.end());
        convertData();
    }

    void Dataset::convertData()
    {
        convertedData.clear();
        convertedData.reserve(trainingData.size());
        for (const auto& data : trainingData)
        {
            std::string converted = convertWord(data);
            if (!converted.empty())
            {
                convertedData.push_back(std::move(converted));
            }
            else
            {
                loaded = false;
                break;
            }
        }        
    }

    std::string Dataset::summary()
    {
        std::stringstream summary("");
        if (loaded)
        {
            summary << "Loaded dataset:" << std::endl;
            summary << "\tAlphabet size: " << alphabet.size() << std::endl;
            summary << "\tAlphabet:";
            for (const auto& ch : alphabet)
                summary << " " << ch;
            summary << std::endl;
            summary << "\tNum samples: " + std::to_string(convertedData.size());
        }
        else
        {
            summary << "Dataset is not loaded, data loader not ready." << std::endl;
        }
        return summary.str();
    }

    std::string Dataset::convertWord(const std::string& alphabeticWord)
    {
        std::string converted(alphabeticWord);
        for (size_t i = 0; i < alphabet.size(); i++)
        {
            std::replace(converted.begin(), converted.end(), alphabet[i], (char)i);
        }
        return converted;
    }

    Dataset* loadDataset(const std::string& datafile)
    {
        std::ifstream file(datafile);
        if (!file)
        {
            std::cout << "Can't open dataset file: " << datafile << std::endl;
            return nullptr;
        }
        nlohmann::json json_data;
        try
        {
            file >> json_data;
        }
        catch (...)
        {
            std::cout << " Errorduring loading json data: incorrect data structure in file " << datafile << std::endl;
            return nullptr;
        }

        DataLoaderType type;
        if (std::string("synthetic").compare(json_data["DatasetFormat"]) == 0)
        {
            type = DataLoaderType::classic;
        }
        else
        {
            std::cout << " Error! Dataset format is not supported:" << json_data["DatasetFormat"] << std::endl;
            return nullptr;
        }

        switch (type)
        {
        case DataLoaderType::classic:
            return new Dataset(json_data);
        default:
            std::cout << "Unsupported DataLoader type!" << std::endl;
            return nullptr;
        }
    }
}

#include "Semiring.h"

namespace genitor2 {

    /**
     * @brief Custom tensormult - Looks like a Matrix-Multiplication with custom addition and multiplication
     *
     * @param x torch tensor
     * @param y torch tensor
     * @return torch tensor
     */
    torch::Tensor Semiring::tensormult(torch::Tensor x, torch::Tensor y) const {
    int dimX = x.size(0);
    int dimY = y.size(1);
    torch::Tensor res = torch::empty({dimX, dimY}, torch::kLong);
    int64_t sum, temp;

    for (size_t i = 0; i < dimX; i++)
    {
        for (size_t j = 0; j < dimY; j++)
        {
            sum = this->_zero;
            for (size_t k = 0; k < x.size(1); k++)
            {
                temp = this->mult(x[i][k].item<int64_t>(), y[k][j].item<int64_t>());
                sum = this->add(sum, temp);
            }
            res[i][j] = sum;
        }
    }
    return res;
    };

    torch::Tensor ClassicSemiring::tensormult(torch::Tensor x, torch::Tensor y) const
    {
        auto res = torch::matmul(x, y);
        // ToDo: Debug code. Remove in final version
        //std::cout << "print res: " << res << std::endl;
        //auto old = Semiring::tensormult(x, y);
        //std::cout << "print old: " << old << std::endl;
        //std::cout << "same result: " << (torch::equal(res, old) ? "yes" : "no") << std::endl;
        //assert(torch::equal(res, old));
        return res;
    }

    torch::Tensor TropicalSemiring::tensormult(torch::Tensor x, torch::Tensor y) const
    {
        auto res = torch::amax(torch::add(x.t(), y), 0, true);
        // ToDo: Debug code. Remove in final version
        //std::cout << "print res: " << res << std::endl;
        //auto old = Semiring::tensormult(x, y);
        //std::cout << "print old: " << old << std::endl;
        //std::cout << "same result: " << (torch::equal(res, old) ? "yes" : "no") << std::endl;
        //assert(torch::equal(res, old));
        return res;
    };
}

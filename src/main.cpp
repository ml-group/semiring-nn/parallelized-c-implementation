#include <torch/torch.h>

#include "GenitorII.h"
#include "Config.h"

using namespace genitor2;

const std::string help("Arguments usage:\n"
                       "\t(required) --config <path> : path to json config file.\n"
                       "\t(required) --epochs <int> : set number of training epochs.\n"
                       "\t(required) --crossover <int> : set crossover length\n"
                       "\t(optional) --static : enable static parent statistic. If not set, dynamic is used.\n"
                       "\t(optional) --directed : enable directed mutation.\n"
                       "\t(optional) --output :  name of output .gv file for automaton(exc. file path, inc. filename without extension)\n"
                       "\t(optional) --draw : draw exported automaton to .png file. Works only with --output arg\n");

const std::string example("Example:\n"
                          "\t(Windows): GenitorII.exe --config /path/to/serialized-c-implementation/data/config.json --epochs 1000 --crossover 2 --static --directed --output automaton --draw\n"
                          "\t(Linux): GenitorII --config /path/to/serialized-c-implementation/data/config.json --epochs 1000 --crossover 2 --static --directed --output automaton --draw\n");

int main(int argc, char* argv[])
{

    int configIdx = -1;
    int epochs = -1;
    int crossoverLength = -1;
    bool chooseParentStatic = false;
    bool directedMutation = false;
    std::string outpath("");
    bool draw = false;

    // Parse input arguments
    std::cout << "Parse input arguments..." << std::endl;
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "--config") == 0)
        {
            configIdx = ++i;
            std::cout << "\tconfig path: " << argv[configIdx] << std::endl;
        }
        else if (strcmp(argv[i], "--epochs") == 0)
        {
            epochs = atoi(argv[++i]);
            std::cout << "\tepochs number: " << epochs << std::endl;
        }
        else if (strcmp(argv[i], "--crossover") == 0)
        {
            crossoverLength = atoi(argv[++i]);
            std::cout << "\tcrossover length: " << crossoverLength << std::endl;
        }
        else if (strcmp(argv[i], "--static") == 0)
        {
            chooseParentStatic = true;
        }
        else if (strcmp(argv[i], "--directed") == 0)
        {
            directedMutation = true;
        }
        else if (strcmp(argv[i], "--output") == 0)
        {
            outpath = argv[++i];
        }
        else if (strcmp(argv[i], "--draw") == 0)
        {
            draw = true;
        }
        else
        {
            std::cout << "Warning! unknown argument is used: " << argv[i] << std::endl;
        }
    }
    std::cout << "\tstatic parent statistic: " << (chooseParentStatic ? "enabled" : "disabled") << std::endl;
    std::cout << "\tdirected mutation: " << (directedMutation ? "enabled" : "disabled") << std::endl;
    
    // Validate input params
    bool valid = true;
    if (configIdx < 1)
    {
        std::cout << "Wrong input arguments: missed config path!" << std::endl;
        valid = false;
    }
    if (epochs < 1)
    {
        std::cout << "Wrong input arguments:  epochs number is incorrect or missed!" << std::endl;
        valid = false;
    }
    if (crossoverLength < 1)
    {
        std::cout << "Wrong input arguments: crossover length number is incorrect or missed!" << std::endl;
        valid = false;
    }
    if (!valid)
    {
        std::cout << "Error! Required arguments are missed or incorrect. Please, restart application with the correct arguments list." << std::endl;;
        std::cout << help << example;
        return 0;
    }
    
    // Start processing
    std::cout << std::endl << "Reading config file..." << std::endl;
    Config config(argv[configIdx]);
    std::cout << std::string(config) << std::endl;

    std::cout << "Creating GenitorII ..." << std::endl;
    GenitorII g(config);

    std::cout << std::endl << "Training ..." << std::endl;
    g.train(epochs, crossoverLength, chooseParentStatic, directedMutation);

    std::cout << std::endl << "Resolving ..." << std::endl;
    auto e = g.final();
    std::cout << "Best individual with fitness = " << round(1000.0 * e.fitness) / 1000.0 << " reached: " << e.nn.linear.view({ 1, -1 }) << std::endl;

    if (!outpath.empty())
    {
        std::string log = "Export ";
        if(draw)
        {
            log += "and draw";
            e.nn.drawAutomaton(outpath);
        }
        else
        {
            e.nn.exportAutomatonToFile(outpath);
        }
        std::cout << log << " best individual automaton: " << outpath  << std::endl;
        
    }

    std::cout << std::endl << "Finished." << std::endl;
    return 0;
}


/** Implementation of a variant of Genitor to train SNNs. */

#include "GenitorII.h"

#include <cmath>
#include <iostream>
#include <chrono>
#include <execution>

namespace genitor2 {

/** Constructor
 * @param _config the configuration parameters as Config structure
 */
    GenitorII::GenitorII(const Config& _config)
        : config(_config)
        , binaryLength(std::floor(std::log2(config.max) + 1))
        , numOfWeights(config.ds->abc().size()* config.nnSize* config.nnSize)
    {
        std::random_device rd; // obtain a random number from hardware
        std::mt19937::result_type seed = rd();
        randomGen = new std::mt19937(seed); // seed the generator

        std::cout << "Using seed " << seed << std::endl;

        population.reserve(config.popSize);
        for (int i = 0; i < config.popSize; i++) {
            population.push_back(Individual(SNN(i, config), -1));
        }

        idNextIndividual = population.size();

        rank(); // initial ranking should update the fitness values of all individuals
    }


/**
 * Destructor
 */
    GenitorII::~GenitorII() noexcept {
        delete randomGen;
    }


/**
 * Convert all chromosomes from binary to decimal representation
 */
    void GenitorII::convertToDecimal() {
        for (int i = 0; i < config.popSize; i++) {
            population[i].nn.convertToDecimal();
        }
    }


    /**
     * Rank the individuals by computing the error and corresponding permutation table.
     * The squared error is used to determine fitness, i.e. we rank by "smallest" error
     * set "ranking" to a permutation table t such that population[i] is at position t[j] if i has the j-th smallest error
     * @return fitness a list of the errors found so far
     */
    void GenitorII::rank() {
        // update the fitness value of all individuals that are not valid anymore
        std::vector<Individual*> updateList;
        updateList.reserve(population.size());
        for (auto& individ : population)
        {
            if (!individ.valid) {
                updateList.push_back(&individ);
            }
        }

        std::for_each(
            std::execution::par,
            updateList.begin(),
            updateList.end(),
            [&](auto&& individ)
            {
                individ->updateFitness(config.ds->data(), config.ds->gt());
            });

        // compute ordering min to max
        std::sort(population.begin(), population.end(), [](const auto& first, const auto& second)
            {
                return first.fitness < second.fitness;
            });
    }


    /**
     * Performs the genetic training algorithm
     * @param epochs the number of iterations to be executed
     * @param crossoverLength the static length of crossOver, 0 if dynamic crossover length is to be used
     * @param chooseParentStatic set to true for static and to false for dynamic parent selection
     * @param directedMutation true iff directed mutation is enabled
     * @return fit the (sorted/ranked) table of errors
     */
    void GenitorII::train(int epochs, int crossoverLength, bool chooseParentStatic, bool directedMutation) {
        int recombs = config.popSize / 2;

        auto startingTime = std::chrono::steady_clock::now();
        //float previousError = population[ranking[0]]->squaredError();
        //int countStagnation = 0; // if too many iterations without improvement, increase mutation rate
        float bestFitness = population.front().fitness;
        std::cout << "Initial error: " << bestFitness << std::endl;
        std::cout << "Top 5 best equations: " << std::endl;
        for (int q = 0; q < 5; q++) {
            print(population[q].nn.linear);
            std::cout << "\tequation error: " << round(1000.0 * population[q].fitness) / 1000.0 << std::endl;
        }

        int size = config.ds->abc().size() * config.nnSize * config.nnSize;
        int binDigits = this->numOfWeights * this->binaryLength;
        const int printStep = std::max(1, (int)(logFrequency * epochs));
        std::cout << "Training log every " << printStep << " epochs" << std::endl;
        for (int i = 0; i < epochs; i++) {
            if (crossoverLength == 0) //crossoverLength length is not set, use dynamic crossover
                crossoverLength = this->crossoverLength(population.front().fitness);

            torch::Tensor offspring = torch::zeros({recombs, size * binaryLength}, torch::kByte);
            int q1 = -2, q2 = -2;
            std::vector<bool> alreadyUsed(config.popSize, false);
            for (int j = 0; j < recombs; j++) {

                if (chooseParentStatic) {
                    q1 += 2;
                    q2 = q1 + 1;
                } else {
                    std::tuple<int, int> q(chooseParents(alreadyUsed));
                    q1 = std::get<0>(q);
                    q2 = std::get<1>(q);
                }

                std::uniform_int_distribution<> dis(0, binDigits - crossoverLength); // define the range
                int rand = dis(*this->randomGen);
                offspring[j] = crossingOver(population[q1].nn.binary, population[q2].nn.binary, rand,
                                            crossoverLength);
            }
            int k = 0;
            for (int j = config.popSize - 1; j >= config.popSize - recombs; j--) {
                population[j].nn.binary = offspring[k];
                population[j].nn.id = idNextIndividual++;
                population[j].invalidateFitness();
                k = k + 1;
            }

            rank(); // update ranking
            if (population.front().fitness < bestFitness) {
                bestFitness = population.front().fitness;
            }

            if (population.front().fitness == 0) {
                std::cout << "Early finish after " << i << " iterations!" << std::endl;
                break;
            } else if (population.front().fitness <= 5 && directedMutation) {
                mutateDirected(population[0], 1);
                rank();
            }

            if ((i + 1) % printStep == 0) {
                std::cout << "-------------------------------"<< std::endl;
                std::cout << "Epoch: " << i + 1 << " out of " << epochs << std::endl;
                std::cout << "Top 5 best equations: " << std::endl;
                for (int q = 0; q < 5; q++) {
                    print(population[q].nn.linear);
                    std::cout << "\tequation error: " << round(1000.0 * population[q].fitness) / 1000.0<< std::endl;
                }
                
                std::cout << "Elapsed time: " << std::chrono::duration_cast<std::chrono::seconds>(
                        std::chrono::steady_clock::now() - startingTime).count() << "s"
                          << std::endl;
                std::cout << "-------------------------------" << std::endl;
            }
        }
        std::cout << "Total running time: ca. " << std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::steady_clock::now() - startingTime).count() << "s." << std::endl;
        std::cout << "Average epoch time: ca. " << std::chrono::duration_cast<std::chrono::seconds>(
            std::chrono::steady_clock::now() - startingTime).count() / float(epochs) << "s." << std::endl;
    }


    /**
     * Dynamically decrease crossover length as the solution quality improves.
     * @param best the error of the best solution found so for
     * @return the crossover length
     */
    int GenitorII::crossoverLength(float best) const {
        int crossoverLength = 12;
        if (best <= 2) {
            crossoverLength = 2;
        } else if (best <= 10) {
            crossoverLength = 3;
        } else if (best <= 20) {
            crossoverLength = 4;
        } else if (best <= 50) {
            crossoverLength = 6;
        } else if (best <= 100) {
            crossoverLength = 8;
        }

        return std::min(crossoverLength, binaryLength);
    }


/**
 * Randomly select parents for the crossOver with a bias towards fitter parents.
 * Within one iteration, no parent is selected twice
 * @param alreadyUsed a table keeping track of the parents already selected for crossing over
 * @return two indices corresponding to the parents chosen for crossing over
 */
    std::tuple<int, int> GenitorII::chooseParents(std::vector<bool> &alreadyUsed) const {
        int parent1, parent2;
        int iteration = 0;
        do {
            parent1 = randomParentIndex();
        } while (alreadyUsed[parent1]);

        do {
            parent2 = randomParentIndex();
        } while (parent1 == parent2 || alreadyUsed[parent2]);

        alreadyUsed[parent1] = true;
        alreadyUsed[parent2] = true;
        return std::tuple<int, int>(parent1, parent2);
    }


/**
 * Randomly choose an index with a bias towards fitter parents
 * @return the index chosen
 */
    int GenitorII::randomParentIndex() const{
        // seed the generator
        std::gamma_distribution<> dis(0.6, 25);
        int rand = dis(*randomGen);
        return rand % config.popSize;
    }


    /**
     * Print a column tensor in readable format
     * @param colTenspr the column tensor to be printed
     */
    void GenitorII::print(const torch::Tensor &colTensor) {
        std::cout << colTensor.view({1, -1}) << std::endl;
    }


    /**
     * Perform the crossing-over between two parents
     * Two parents are combined by crossing-over (i.e. swapping) their binary encodings
     * corresponding beginning at position pos.
     * This produces two offsprings one of which is randomly chosen and returned.
     * @param parent1 the first parent
     * @param parent2 the second parent
     * @param pos >= 1 the position, i.e. weight
     * @param length length of the crossover
     * @return offspring (copy of) one of the parents (chosen randomly)
     */
    torch::Tensor GenitorII::crossingOver(const torch::Tensor &parent1, const torch::Tensor &parent2, int pos, int length) const {
        std::uniform_int_distribution<> dis(1, 2); // define the range
        int rand = dis(*randomGen);
        torch::Tensor offspring;

        if (rand == 1) { // choose parent 1 as base and insert parts of parent 2
            offspring = parent1.detach().clone();;
            for (int i = pos; i < pos + length; i++) {
                offspring[i] = parent2[i];
            }
        } else { // choose parent 2 as base and insert parts of parent 1
            offspring = parent2.detach().clone();;
            for (int i = pos; i < pos + length; i++) {
                offspring[i] = parent1[i];
            }
        }

        int distance = hammingDistance(parent1, parent2);
        mutate(offspring, (int) (offspring.size(0) - distance) / 4);
        return offspring;
    }


    /**
     * Compute the Hamming distance between two chromosomes represented as 1D tensors
     * @param tensor1 the first tensor
     * @param2 tensor2 the 2nd tensor
     * @return the Hamming distance between the input tensors
     */
    int GenitorII::hammingDistance(const torch::Tensor &tensor1, const torch::Tensor &tensor2) {
        int dist = 0;
        for (int i = 0; i < tensor1.size(0); i++) {
            if (tensor1[i].item<uint8_t>() != tensor2[i].item<uint8_t>())
                dist++;
        }
        return dist;
    }


    /**
     * Perform the mutation operation on a chromosome
     * @param tensor the chromosome represented as 1D tensor to be mutated
     * @param numMutations the number of mutations, i.e. bits to be flipped
     * @return the tensor after mutation with some bits flipped
     */
    void GenitorII::mutate(torch::Tensor &tensor, int numMutations) const {
        for (int k = 0; k < numMutations; k++) {
            std::uniform_int_distribution<> dis(0, (int) (tensor.size(0) - 1)); // define the range
            int j = dis(*randomGen);
            tensor[j] = tensor[j].item<uint8_t>() == 0 ? 1 : 0;
        }
    }


    /**
     * Perform a directed mutation operation on the net
     * @param net the net to apply directed mutation on (usually best-ranked pop member)
     * @param replacement the offset index of the pop member which shall be replaced by directed mutation (usualy a low-ranked member - use replacement = 1 for last member, higher values for better ranked members)
     * @return the chromosome represented as 1D tensor after directed mutation
     */
    void GenitorII::mutateDirected(const Individual& net, int replacement) {
        float previousBest = net.fitness;
        assert(config.popSize >= replacement);
        Individual* target = &population[config.popSize - replacement];
        target->nn.linear = net.nn.linear.detach().clone();;

        // adjust the target step by step by replacing its components with the mutated ones from net
        for (int j = 0; j < target->nn.linear.size(1); j++) {
            float targetFitness = target->fitness;

            target->nn.linear[j] = net.nn.linear[j] + 1;
            target->fitness = target->nn.squaredError(config.ds->data(), config.ds->gt());

            if (target->fitness > previousBest) {
                // target performs WORSE than net (bigger error) -> adjust
                target->nn.linear[j] = net.nn.linear[j] - 2;
                target->fitness = target->nn.squaredError(config.ds->data(), config.ds->gt());
            }

            if (target->fitness > previousBest) {
                // target still performs WORSE than net -> undo target changes
                target->nn.linear[j] = target->nn.linear[j] + 1;
                target->fitness = targetFitness;
            } else {
                // target performs better or equal to net
                break;
            }
        }
        target->nn.convertToBinary();
    }


    /**
    * Returns the best-ranking individual
    * After training is successfully completed, this is the final NN
    * @return the population member with the least squared error
    */
    GenitorII::Individual GenitorII::final() const {
        return population[0];
    }

    /**
     * invalidate the fitness values of all the individuals in the population
     */
    void GenitorII:: invalidateAllFitnessValues(){
        for(auto& indiv: population) {
                indiv.invalidateFitness();
        }
    }

}//namespace
#ifndef GENETIC_H
#define GENETIC_H

#include <random>
#include <vector>
#include <string>

#include <torch/torch.h>

#include "SNN.h"
#include "Config.h"

namespace genitor2 {
    

    class GenitorII {
    public:
        struct Individual {
            SNN nn;
            float fitness;
            bool valid;
            Individual(const SNN& _nn, float _fitness)
                : nn(_nn)
                , fitness(_fitness)
                , valid(false)
            {};

            void updateFitness(const std::vector<std::string>& convertedData, const std::vector<float>& testData)
            {
                nn.convertToDecimal();
                fitness = nn.squaredError(convertedData, testData);
                valid = true;
            };

            void invalidateFitness(){
                valid = false;
            }
        };

        GenitorII(const Config &config);

        ~GenitorII() noexcept;

        void train(int epochs, int crossoverLength, bool chooseParentStatic, bool directedMutation);

        Individual final() const;

        int crossoverLength(float best) const;

        std::tuple<int, int> chooseParents(std::vector<bool> &alreadyUsed) const;

        int randomParentIndex() const;

        static void print(const torch::Tensor &colTensor);

        torch::Tensor crossingOver(const torch::Tensor &parent1, const torch::Tensor &parent2, int pos, int length) const;

        static int hammingDistance(const torch::Tensor &tensor1, const torch::Tensor &tensor2);

        void mutate(torch::Tensor &tensor, int numMutations) const;

        void invalidateAllFitnessValues();

    protected:
        void mutateDirected(const Individual &net, int replacement);

        void convertToDecimal();

        void rank();

    protected:
        const Config config;
        const int binaryLength;
        const int numOfWeights;
        std::vector <Individual> population;
        int idNextIndividual;
        std::mt19937 *randomGen;
        const float logFrequency = 0.1f;
    };
}
#endif // !GENETIC_H
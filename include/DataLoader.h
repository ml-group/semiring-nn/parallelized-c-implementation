#ifndef DATALOADER_H
#define DATALOADER_H

#include <vector>
#include <string>
#include "nlohmann/json.hpp"

#include "Semiring.h"

namespace genitor2 {
    enum DataLoaderType {classic};

    class Dataset {
    protected:
        std::vector<char> alphabet;
        std::vector<std::string> trainingData;
        std::vector<std::string> convertedData;
        std::vector<float> testData;
        bool loaded;

        Dataset() = default;
        void Dataset::convertData();
    public:
        Dataset(const std::string& datafile);
        Dataset(const nlohmann::json& json_data);
        Dataset(std::vector<char> _alphabet, std::vector<std::string> _trainingData, std::vector<float> _testData);
        const std::vector<char>& abc() const { return alphabet; }
        const std::vector<std::string>& data() const { return convertedData; }
        const std::vector<std::string>& originalData() const { return trainingData; }
        const std::vector<float>& gt() const { return testData; }
        bool isLoaded() const { return loaded; }
        std::string summary();
        std::string convertWord(const std::string& alphabeticWord);
    };

    Dataset* loadDataset(const std::string& datafile);
}
#endif // !DATALOADER_H

#ifndef EQUATIONS_H
#define EQUATIONS_H

#include <vector>

#include <torch/torch.h>

#include "Semiring.h"
#include "Config.h"

namespace genitor2 {
    // Implements a population member (SNN) for the Genitor algorithm.
    class SNN {

    protected:
        const Semiring* semiring;
        int weightsSize;
        int alphabetSize;
        int numStates;
        int binaryLength;
        int maxNum;

    public:
        int id;
        torch::Tensor weights;
        torch::Tensor binary;
        torch::Tensor linear;


        SNN(int id, const Config &config);

        //uses arr as output param
        static void convertIntToBinary(unsigned int num, int bits, int pos, std::vector<int> &arr);

        void convertToDecimal();
        void convertToBinary();

        void setSemiring(Semiring* s);

        torch::Tensor randomMatrix(int alphabet, int min, int max, int n);

        torch::Tensor zeroMatrix(int alpha, int n);

        torch::Tensor oneMatrix(int alpha, int n);

        void setWeights(torch::Tensor weightTensor);

        torch::Tensor getWeights();

        void setWeightsFromLinear();

        void setNumberOfStates(int stateNum);

        int getNumberOfStates();

        double getStateValue(int i, const std::string &word);

        double forward(const std::string &word); //not sure

        float squaredError(const std::vector<std::string> &convertedData, const std::vector<float> &testData);

        void printWeights(int alpha);

        void printAutomaton();

        void exportAutomatonToFile(const std::string &name);

        void drawAutomaton(const std::string &name);

        void printStatesAfter(const std::string &word);

        ~SNN();
    };

}
#endif // !EQUATIONS_H

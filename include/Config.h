#ifndef CONFIG_H
#define CONFIG_H

#include <vector>
#include <string>
#include <fstream>

#include "nlohmann/json.hpp"
#include "Semiring.h"
#include "DataLoader.h"

namespace genitor2 {

    struct Config {
        Dataset* ds;
        SemiringType semiring;
        size_t popSize;
        size_t nnSize;
        int min;
        int max;

        Config(int _min, int _max, size_t _popSize, size_t _nnSize, SemiringType _semiring = SemiringType::classic)
            : min(_min)
            , max(_max)
            , popSize(_popSize)
            , nnSize(_nnSize)
            , semiring(_semiring)
            , ds(nullptr)
        {
        };

        Config(const char* configPath)
        {
            // Open config file
            std::ifstream file(configPath);
            if (!file) {
                throw std::runtime_error("Error! Can't open config file!");
            }

            // parse json data
            nlohmann::json json_data;
            file >> json_data;

            // initialize config using data from json
            min = json_data["min"];
            max = json_data["max"];
            popSize = json_data["popSize"];
            nnSize = json_data["nnSize"];
            semiring = mSemiringSelector.typeFromStr(json_data["Semiring"]);

            if (SemiringType::unknown == semiring)
            {
                semiring = SemiringType::classic;
                std::cout << "Error! Can't find semiring specified: " << json_data["Semiring"]
                    << ". Used " << mSemiringSelector.getSemiringName(semiring) << std::endl;
                
            }

            // Load dataset
            ds = loadDataset(json_data["dataset"]);
            if (nullptr == ds)
            {
                throw std::runtime_error("Error! Can't process dataset file!");
            }
            if (!ds->isLoaded())
            {
                throw std::runtime_error("Error! Data is not loaded!");
            }
        }

        ~Config()
        {
            delete ds;
        }

        Config(Config &&other) = default;

        Config(const Config& other) = default;

        operator std::string()
        {
            std::stringstream ss("");
            ss << "Config:" << std::endl;
            ss << "\tpopSize: " << popSize << std::endl;
            ss << "\tnnSize: " << nnSize << std::endl;
            ss << "\tmin: " << min << std::endl;
            ss << "\tmax: " << max << std::endl;
            ss << "\tSemiring: " << mSemiringSelector.getSemiringName(semiring) << std::endl;
            ss << ds->summary() << std::endl;
            return ss.str();
        }
    };

}

#endif // !CONFIG_H

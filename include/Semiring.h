#ifndef SEMIRING_H
#define SEMIRING_H

#include <string>
#include <map>

#include <torch/torch.h>

namespace genitor2 {
    enum class SemiringType { unknown = -1, classic, tropical };

    /**
     * Defines a semiring class to instantiate different semirings
     * @class Semiring
     * Each class instance represents a specific semiring, implementing
     * its addition and multiplication maps and, if it is a complete semiring,
     * a closure operation. Currently, all semirings in this implementation use
     * the real numbers as base. Additional elements for infinity can be passed as
     * the extension table.
     */
    class Semiring {
    protected:
        int64_t _zero;
        int64_t _one;

    public:
        Semiring() = default;
        Semiring(int64_t __zero, int64_t __one)
            : _zero(__zero)
            , _one(__one)
        {};
        ~Semiring() = default;

        const int64_t& one() const { return _one; }
        const int64_t& zero() const { return _zero; }

        virtual int64_t add(int64_t first, int64_t second) const = 0;
        virtual int64_t mult(int64_t first, int64_t second) const = 0;
        virtual torch::Tensor tensormult(torch::Tensor x, torch::Tensor y) const;
        virtual SemiringType getType() const { return SemiringType::unknown; }
    };

    class ClassicSemiring : public Semiring
    {
    public:
        ClassicSemiring()
            : Semiring(0, 1)
        {};
        ClassicSemiring(int64_t __zero, int64_t __one)
            : Semiring(__zero, __one)
        {};
        ~ClassicSemiring() = default;

        virtual int64_t add(int64_t first, int64_t second) const override { return first + second; }
        virtual int64_t mult(int64_t first, int64_t second) const override { return first * second; }
        virtual torch::Tensor tensormult(torch::Tensor x, torch::Tensor y) const override;
        virtual SemiringType getType() const { return SemiringType::classic; }
    };

    class TropicalSemiring : public Semiring
    {
    public:
        TropicalSemiring()
            : Semiring(INT32_MIN, 0)
        {};
        TropicalSemiring(int64_t __zero, int64_t __one)
            : Semiring(__zero, __one)
        {};
        ~TropicalSemiring() = default;

        virtual int64_t add(int64_t first, int64_t second) const override { return first > second ? first : second; }
        virtual int64_t mult(int64_t first, int64_t second) const override { return first + second; }
        virtual torch::Tensor tensormult(torch::Tensor x, torch::Tensor y) const override;
        virtual SemiringType getType() const override { return SemiringType::tropical; }
    };

    class SemiringSelector
    {
    private:
        const std::map<SemiringType, Semiring*> semirings = {
                std::pair<SemiringType, Semiring*>(SemiringType::classic,  new ClassicSemiring()),
                std::pair<SemiringType, Semiring*>(SemiringType::tropical,  new TropicalSemiring()) };

        const  std::map<SemiringType, std::string> semiringNames = { std::pair<SemiringType, std::string>(SemiringType::classic,  "classic"),
                                                                     std::pair<SemiringType, std::string>(SemiringType::tropical,  "tropical") };
    public:
        ~SemiringSelector()
        {
            for (auto semiring : semirings)
            {
                delete semiring.second;
            }
        }

        const char* getSemiringName(SemiringType st)
        {
            return semiringNames.find(st) != semiringNames.end() ? semiringNames.at(st).c_str() : "Unknown";
        }

        SemiringType typeFromStr(const std::string& typestr)
        {
            for (const auto st : semiringNames)
            {
                if (st.second.compare(typestr) == 0)
                    return st.first;
            }
            return SemiringType::unknown;
        }

        const Semiring* getSemiring(SemiringType st)
        {
            return semirings.find(st) != semirings.end() ? semirings.at(st) : nullptr;
        }

    };

    static SemiringSelector mSemiringSelector;
}
#endif // !SEMIRING_H

# A Parallelized Genitor II Implementation For Training Semiring Neural Networks
# Introduction
Semiring Neural Networks(SNNs) have been introduced as a recurrent neural network-type representation of weighted automata with the potential to recover a recognizable series in sequential data(e.g. time series).

They have the potential as an explainable alternative to Recurrent Neural Networks as well as an approach for data-driven recovering of Weighted Finite Automatona for a given task.

However, due to integer weights used for counting tasks which are the main focus now, the gradient descent-based approaches cannot be used to train SNN. As an alternative, the genetic algorithm solution was proposed.

This implementation provides the fast and efficient training of semiring neural networks even for relatively large network sizes of several hundreds and supports investigation of SNNs potential.

## Papers
**GECCO 2023 Conference Poster**

- A Parallelized Genitor II Implementation For Training Semiring Neural Networks
*Olena Horokh, Martin Böhm, Thomas Schmid*
(the link will be added soon)

**Related research**
- [An Algorithmic Approach to Establish a Lower Bound for the Size of Semiring Neural Networks](https://www.esann.org/sites/default/files/proceedings/2021/ES2021-33.pdf)
*M. Böhm, T. Schmid; Proceedings of the 29th European Symposium on Artificial Neural Networks, Computational Intelligence and Machine Learning (ESANN), p. 311-316, 2021*

# Getting started
## Installation

**Requirements**

To build the project you need to install the requirements first. Please, make sure you have installed:
- [CMake](<https://cmake.org/install/>) Tested on version: 3.19.0
- [PyTorch C++ API](<https://pytorch.org/get-started/locally/>).  Tested on version: prebuilt binaries 1.10.1 for CPU-only | libtorch-gpu-11.3 for GPU
- C/C++ compiler, tested using MSVC version 19.29.30146.0 (VisualStudio 2019)

## Build & Run
TBD soon

**Multiple-experiments-run script: GECCO2023 Poster experiments**

TBD soon

# Contacts:
 - **Olena Horokh** (olena.horokh@tu-dresden.de) - implemenration details & code run
 - ~~**Martin Böhm** (martin.boehm@medizin.uni-halle.de) - SNN theory & perspective~~ Please, contact Thomas Schmid instead
 - **Thomas Schmid** (schmid@informatik.uni-leipzig.de) - further research activities & collaboration

# License:
The implementation is shared under the **MIT license** (see the LICENSE file for details). 

However, also pay attention to the third-party dependencies that may require a different license:
- [nlohmann/json](https://github.com/nlohmann/json) - **MIT License** (used for config|dataset files support and stored as include/nlohmann/json.hpp)
- [PyTorch](https://github.com/pytorch/pytorch) - **BSD-style license**, see their LICENSE file. (used for matrix-vector operations)

# Acknowledgments:
We would like to thank Adrian Schönnagel, Jimmy Pöhlmann, Charles Alrabbaa, Christopher Brunner, and Leah Sieder, who also participated in transferring the original prototype implementation by Martin Böhm from LUA to C++ and preparing an initial version.
